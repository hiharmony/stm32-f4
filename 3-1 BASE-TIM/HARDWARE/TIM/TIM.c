#include "tim.h"
#include "stm32f4xx.h" 



void TIM_Init(void)
{
		TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStructure;
		NVIC_InitTypeDef NVIC_InitStructure;
		//时钟使能
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM6,ENABLE);
		//定时器初始化 时钟源84MHZ
		TIM_TimeBaseInitStructure.TIM_Prescaler = 8400-1; //预分配系数8400，即T=1/10000=0.1ms
		TIM_TimeBaseInitStructure.TIM_Period = 5000-1; //0.1ms*5000=500ms
		TIM_TimeBaseInit(TIM6,&TIM_TimeBaseInitStructure);
		//清除定时器中断标志位
		TIM_ClearFlag(TIM6,TIM_FLAG_Update);
		//使能定时器中断
		TIM_ITConfig(TIM6,TIM_IT_Update,ENABLE);
		//使能定时器
		TIM_Cmd(TIM6,ENABLE);
		//配置中断
		NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
		NVIC_InitStructure.NVIC_IRQChannel = TIM6_DAC_IRQn;
		NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
		NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=1;
		NVIC_InitStructure.NVIC_IRQChannelSubPriority=1;
		NVIC_Init(&NVIC_InitStructure);
}





