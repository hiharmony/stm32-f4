#include "WWDG.h"
#include "stm32f4xx.h" 


void WWDG_Init(void)
{
		NVIC_InitTypeDef NVIC_InitTStruture;
		//使能时钟
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_WWDG,ENABLE);
		//设置分频系数
		WWDG_SetPrescaler(WWDG_Prescaler_8);
		//设置上窗口值
		WWDG_SetWindowValue(0x5f);
		//开启提前唤醒中断
		WWDG_EnableIT();
		//中断分组和初始化中断
		NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
		NVIC_InitTStruture.NVIC_IRQChannel=WWDG_IRQn;
		NVIC_InitTStruture.NVIC_IRQChannelCmd= ENABLE;
		NVIC_InitTStruture.NVIC_IRQChannelPreemptionPriority=1;
		NVIC_InitTStruture.NVIC_IRQChannelSubPriority=1;
		NVIC_Init(&NVIC_InitTStruture);
		//清除中断标志位
		WWDG_ClearFlag();
		//使能看门狗
		WWDG_Enable(0x7F&0x7F);
		//喂狗
		WWDG_SetCounter(0x7F&0x7F);
		
}

void WWDG_IRQHandler()
{
		WWDG_SetCounter(0x7F&0x7F);
		WWDG_ClearFlag();
		GPIO_ToggleBits(GPIOF,GPIO_Pin_9);
}









