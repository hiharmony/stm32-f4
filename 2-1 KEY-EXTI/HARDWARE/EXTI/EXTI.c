#include "exti.h"
#include "key.h"
#include "stm32f4xx.h" 



void EXTI_User_Init(void)
{
		EXTI_InitTypeDef EXTI_InitStructure;
		NVIC_InitTypeDef NVIC_InitStructure;
		//时钟使能
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
		//按键GPIO初始化
		KEY_Init();
		//设置IO口与中断线映射关系
		SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOE,EXTI_PinSource2);
		//初始化线上中断
		EXTI_InitStructure.EXTI_Line = EXTI_Line2;
		EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
		EXTI_InitStructure.EXTI_Trigger =EXTI_Trigger_Falling;
		EXTI_InitStructure.EXTI_LineCmd = ENABLE;
		EXTI_Init(&EXTI_InitStructure);
		//中断分组
		NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
		//中断优先级初始化
		NVIC_InitStructure.NVIC_IRQChannel=EXTI2_IRQn;
		NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=1;
		NVIC_InitStructure.NVIC_IRQChannelSubPriority=1;
		NVIC_InitStructure.NVIC_IRQChannelCmd=ENABLE;
		NVIC_Init(&NVIC_InitStructure);
}


