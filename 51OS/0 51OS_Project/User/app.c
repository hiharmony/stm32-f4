#include "app.h"


//任务1 资源
char idata thread_a_stack[30]; //存放到片内ram
thread_block thread_a;
void thread_a_fun(void* agrv);
//任务2 资源
char idata thread_b_stack[30];
thread_block thread_b;
void thread_b_fun(void* agrv);
//任务3 资源
char idata thread_c_stack[30];
thread_block thread_c;
void thread_c_fun(void* agrv);

void main(void)
{
		BSP_Init(); //板级硬件初始化
		
		//任务1 LED1闪烁
		thread_create(&thread_a,thread_a_fun,thread_a_stack);
		//任务2 LED2闪烁
		thread_create(&thread_b,thread_b_fun,thread_b_stack);
		//任务3 串口发送
		thread_create(&thread_c,thread_c_fun,thread_c_stack);
		Timer0_interupt_enable();//开定时器中断
		while(1);
}


#define Test 0
void BSP_Init()
{
		UsartInit();
		Timer0_set_callback(thread_change); //任务调度函数指针赋值
		Timer0_Init(); //开启定时器时基，不断的调用任务函数
		Timer0_interupt_disable(); //任务还没初始化，暂时屏蔽定时器中断
#if Test
		LED0_ON;
		LED1_ON;
		while(1)
		{
				putchar('1');
				Delay500ms();
		}

#endif
}

//线程A
void thread_a_fun(void* agrv)
{
		u16 ia=0;
		u8 flaga=0;
		while(1)
		{	
				
				if(flaga==0)
				{
						ia++;
						if(ia==1000)
						{
								LED0_ON;
								flaga=1;
						}
				}
				else
				{
						
						ia--;
						if(ia==0)
						{
								
								LED0_OFF;
								flaga=0;
						}
				}	
				//printchar("task1\r\n");
		}
}


//线程B
void thread_b_fun(void* agrv)
{
		u16 ib=0;
		u8 flagb=0;
		while(1)
		{	
				
				if(flagb==0)
				{
						ib++;
						if(ib==1000)
						{
								LED1_OFF;
								flagb=1;
						}
				}
				else
				{
						
						ib--;
						if(ib==0)
						{
								
								LED1_ON;
								flagb=0;
						}
				}
				
				//printchar("task2\r\n");
		}

}


//线程C
void thread_c_fun(void* agrv)
{
		int c = 0;
		while(1)
		{
				printchar("task3\r\n");
				//printf("task3\r\n");
		}
}




 









