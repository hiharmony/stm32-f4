#ifndef _OS_H
#define _OS_H

#include "app.h"

#define THREAD_LIST_LEN						5
#define THREAD_CHANGE_PUSH_BYTES	2

//线程信息块
 typedef void(*thread_pfun)(void *argv);//任务函数指针
typedef struct thread_block
{
	char PC_L;
	char PC_H;
	char ACC;
	char B;
	char DPH;
	char DPL;
	char PSW;
	char R0;
	char R1;
	char R2;
	char R3;
	char R4;
	char R5;
	char R6;
	char R7;
	char sp;
	thread_pfun thread_fun;
}thread_block;

 
//线程管理列表
typedef struct thread_manage_list
{
		thread_block* list[THREAD_LIST_LEN]; //线程
		int list_index;											 //线城索引
		// 当前运行的线程和下一个要运行的线程
		thread_block*   	thread_cur  ;      //当前线程
		thread_block*    	thread_next ;      //下一个线程
}thread_manage_list;

void thread_create(thread_block* block,thread_pfun pfun,char* sp_init);
char thread_change(void); 

#endif