#include "os.h"

//全局线程管理列表。注意，列表里面应至少拥有一个线程
thread_manage_list idata  g_thread_manage_list = {{0},0,0,0}; //线程，线程索引，当前线程块，下一个线程块


 //线程创建:创建一个线程之后立即投入运行
void thread_create(thread_block* block,thread_pfun pfun,char* sp_init)
{
		block->PC_L =  (unsigned int)pfun%256 ; //即函数指针
		block->PC_H =  (unsigned int)pfun/256 ; //PC指针是16位的，所以拆分位两个8位的数据存储
		block->sp = sp_init; //SP指针,栈区指针
		g_thread_manage_list.list[g_thread_manage_list.list_index] = block; //当前任务信息块地址存进去
		g_thread_manage_list.list_index++; 
	}

//线程调度 顺序切换
thread_block* thread_schedule(void)
{
		static char i = 0;
		i++;
		if(i >= g_thread_manage_list.list_index)
		{
			i = 0;
		}
		
		return g_thread_manage_list.list[i];
}


//函数:线程切换。从thread_cur切换到thread_next。当thread_cur = 0表明处在main线程。
//这个函数一般在中断中调用。这个中断周期作为操作系统运行的时基
char thread_change(void)
{
		char *my_sp =(char *)SP;
		thread_block* p_th_block = 0;
		my_sp-=THREAD_CHANGE_PUSH_BYTES; //减去函数调用自身的压栈
		
		//thread_cur == 0时是从main线程进入中断的
		p_th_block = g_thread_manage_list.thread_cur; //当前任务保存
		if(p_th_block != 0)
		{
				//把中断现场保存到cur
				p_th_block->sp	= (char)(my_sp - 15);
				p_th_block->R7 	= *(my_sp);
				p_th_block->R6 	= *(my_sp-1);
				p_th_block->R5 	= *(my_sp-2);
				p_th_block->R4 	= *(my_sp-3);
				p_th_block->R3 	= *(my_sp-4);
				p_th_block->R2 	= *(my_sp-5);
				p_th_block->R1 	= *(my_sp-6);
				p_th_block->R0 	= *(my_sp-7);
				p_th_block->PSW = *(my_sp-8);
				p_th_block->DPL = *(my_sp-9);
				p_th_block->DPH = *(my_sp-10);
				p_th_block->B 	= *(my_sp-11);
				p_th_block->ACC = *(my_sp-12);
				p_th_block->PC_H= *(my_sp-13);
				p_th_block->PC_L= *(my_sp-14); 
		}
	 
		//把next的现场当成中断现场
		if(g_thread_manage_list.thread_next == 0)
		{
				g_thread_manage_list.thread_next = g_thread_manage_list.list[0];
		}

		p_th_block = g_thread_manage_list.thread_next; //导出下一个环境
		
		my_sp 	   = (char *)(p_th_block->sp + 15);
		*(my_sp)   = p_th_block->R7 ;
		*(my_sp-1) = p_th_block->R6 ;
		*(my_sp-2) = p_th_block->R5 ;
		*(my_sp-3) = p_th_block->R4 ;
		*(my_sp-4) = p_th_block->R3 ;
		*(my_sp-5) = p_th_block->R2 ;
		*(my_sp-6) = p_th_block->R1 ;
		*(my_sp-7) = p_th_block->R0 ;
		*(my_sp-8) = p_th_block->PSW ;
		*(my_sp-9) = p_th_block->DPL ;
		*(my_sp-10)= p_th_block->DPH ;
		*(my_sp-11)= p_th_block->B ;
		*(my_sp-12)= p_th_block->ACC ;
		*(my_sp-13)= p_th_block->PC_H;
		*(my_sp-14)= p_th_block->PC_L;
				
		g_thread_manage_list.thread_cur = g_thread_manage_list.thread_next; //切换变量
		g_thread_manage_list.thread_next = thread_schedule(); 
		
		return (char)my_sp; 
}












