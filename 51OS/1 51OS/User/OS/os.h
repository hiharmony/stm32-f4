#ifndef _OS_H
#define _OS_H

#include "app.h"

#define TASK_LIST_LEN	5 //最大任务数量
#define TASK_CHANGE_PUSH_BYTES 2 //调用自身的栈空间

//线程信息块
typedef void(*taskFun)(void *argv);//任务函数指针
typedef struct task_block
{
	char PC_L,PC_H,ACC,B,DPTR_H,DPTR_L,PSW,R0,R1,R2,R3,R4,R5,R6,R7,sp;
}TaskBlock;


//任务管理列表
typedef struct task_manage_list
{
		TaskBlock* list[TASK_LIST_LEN]; 	//任务块数组
		char list_num;									  //任务块数量
		char list_cur_index;							//当前任务块索引
		char list_next_index;							//当前任务块索引
}TaskManageList;



void taskCreate(TaskBlock* block,taskFun pfun,char* sp_init);
char taskChange(void); 

#endif