#include "os.h"

TaskManageList idata  task_manage_list = {{0},0,0,0}; //全局管理

void taskCreate(TaskBlock* block,taskFun pfun,char* sp_init)
{
		block->PC_L =  (unsigned int)pfun%256 ; //第一次是函数指针
		block->PC_H =  (unsigned int)pfun/256 ; //PC指针是16位的，所以拆分位两个8位的数据存储
		block->sp = sp_init; //SP指针,栈区指针
		task_manage_list.list[task_manage_list.list_num]=block; //任务块存储
		task_manage_list.list_num++; //任务数量+1
}


//函数:线程切换
char taskChange(void)
{
		char *sp_cur=(char *)SP; //取出进入中断后的指针，此时存储栈区存储着工作信息
		TaskBlock* temp_block=0; //临时信息块
		sp_cur-=TASK_CHANGE_PUSH_BYTES; //减去函数调用自身的压栈

		//保存上一次的任务工作环境
		
		if(task_manage_list.list_cur_index!=0) //存入当前工作状态，第一次跳过
		{
				temp_block = task_manage_list.list[task_manage_list.list_cur_index-1]; //取出当前信息块
				temp_block->sp  	= (char)(sp_cur - 15);
				temp_block->R7  	= *(sp_cur);
				temp_block->R6  	= *(sp_cur-1);
				temp_block->R5  	= *(sp_cur-2);
				temp_block->R4  	= *(sp_cur-3);
				temp_block->R3  	= *(sp_cur-4);
				temp_block->R2  	= *(sp_cur-5);
				temp_block->R1  	= *(sp_cur-6);
				temp_block->R0  	= *(sp_cur-7);
				temp_block->PSW  	= *(sp_cur-8);
				temp_block->DPTR_L= *(sp_cur-9);
				temp_block->DPTR_H= *(sp_cur-10);
				temp_block->B 		= *(sp_cur-11);
				temp_block->ACC 	= *(sp_cur-12);
				temp_block->PC_H	= *(sp_cur-13);
				temp_block->PC_L	= *(sp_cur-14); 
		}
			
		//取出下一次任务工作环境
		if(0==task_manage_list.list_next_index) //如果是第一次，加载列表里面第一个
		{
				task_manage_list.list_next_index=1;
		}
		temp_block = task_manage_list.list[task_manage_list.list_next_index-1]; //取出下一次信息块
		sp_cur 			= (char *)(temp_block->sp+15);
		*(sp_cur) 	= temp_block->R7;
		*(sp_cur-1)	= temp_block->R6;
		*(sp_cur-2)	= temp_block->R5;
		*(sp_cur-3)	= temp_block->R4;
		*(sp_cur-4)	= temp_block->R3;
		*(sp_cur-5)	= temp_block->R2;
		*(sp_cur-6)	= temp_block->R1;
		*(sp_cur-7)	= temp_block->R0;	
		*(sp_cur-8)	= temp_block->PSW;
		*(sp_cur-9)	= temp_block->DPTR_L;
		*(sp_cur-10)=	temp_block->DPTR_H; 
		*(sp_cur-11)=	temp_block->B;
		*(sp_cur-12)=	temp_block->ACC;
		*(sp_cur-13)=	temp_block->PC_H;
		*(sp_cur-14)=	temp_block->PC_L; 		
		
		//刷新索引数据，轮流执行
		task_manage_list.list_cur_index  = task_manage_list.list_next_index;
		task_manage_list.list_next_index = (task_manage_list.list_next_index%task_manage_list.list_num)+1;
		
		return (char)sp_cur; 
}












