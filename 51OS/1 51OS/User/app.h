#ifndef APP_h
#define APP_h
#include <reg52.h>
#include "uart.h"
#include "timer.h"
#include "os.h"

sbit D1=P1^0;
sbit D2=P1^1;
#define LED0_ON D1=0
#define LED0_OFF D1=1
#define LED1_ON D2=0
#define LED1_OFF D2=1

typedef unsigned int u16;	  //对数据类型进行声明定义
typedef unsigned char u8;


void BSP_Init(); //板级硬件初始化



#endif






