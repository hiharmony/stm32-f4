#include "timer.h"

timer0_callback p_timer0_callback=0;

//时钟0初始化
void Timer0_Init()
{
 
	TL0 = (65536 - TIMER_RELOAD )%256;   
	TH0 = (65536 - TIMER_RELOAD )/256;
	TMOD |=0x01;
	ET0 = 1;
	EA=1;
	TR0 = 0;
	TR0 = 1;
	
}

//开定时器中断
void Timer0_interupt_enable()
{
		ET0 = 1;
	
}
//关定时器中断
void Timer0_interupt_disable()
{
		ET0 = 0;
	
}

//任务函数指针赋值
void Timer0_set_callback(void* pfun)
{
    p_timer0_callback = (timer0_callback)pfun;
}


void timer0_irq() interrupt 1
{
		EA = 0;
		TL0 = (65536 - TIMER_RELOAD )%256;   
		TH0 = (65536 - TIMER_RELOAD )/256;
		
		if(p_timer0_callback != 0)
		{
				SP = p_timer0_callback(); //调用线程切换
		}
		EA = 1;
}

void Delay500ms()		//@11.0592MHz
{
	unsigned char i, j, k;

	_nop_();
	_nop_();
	i = 22;
	j = 3;
	k = 227;
	do
	{
		do
		{
			while (--k);
		} while (--j);
	} while (--i);
}

void Delay100us()		//@11.0592MHz
{
	unsigned char i, j;

	_nop_();
	_nop_();
	i = 2;
	j = 15;
	do
	{
		while (--j);
	} while (--i);
}


