#ifndef _TIMER_H
#define _TIMER_H

#include "app.h"
#include "intrins.h" 
#define TIMER_RELOAD    30000


typedef char (*timer0_callback)(void); //函数指针
extern timer0_callback p_timer0_callback; //初始化函数指针


void Timer0_Init();
void Timer0_interupt_enable();
void Timer0_interupt_disable();
void Timer0_set_callback(void* pfun);

void Delay500ms();
void Delay100us();






void Delay1000ms();



#endif