#include "PWM.h"
#include "stm32f4xx.h" 


void PWM_Init(void)
{
		TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruture;
		TIM_OCInitTypeDef TIM_OCInitStructure;
		//时钟使能
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM14,ENABLE);
		//初始化定时器
		TIM_TimeBaseInitStruture.TIM_Prescaler = 84-1; //分频
		TIM_TimeBaseInitStruture.TIM_Period = 500-1; //自动装载值arr=500 频率为2KHZ
		TIM_TimeBaseInitStruture.TIM_CounterMode = TIM_CounterMode_Up; //向上计数模式
		TIM_TimeBaseInitStruture.TIM_ClockDivision = TIM_CKD_DIV1; //不分频
		TIM_TimeBaseInit(TIM14,&TIM_TimeBaseInitStruture);
		//输出比较初始化
		TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1; // PWM1模式 小于CCR时是有效电平
		TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_Low; //有效电平为低电平
		TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable; //比较输出使能
		TIM_OC1Init(TIM14,&TIM_OCInitStructure);
		//使能arr
		TIM_ARRPreloadConfig(TIM14,ENABLE);
		//使能预装载寄存器CCR
		TIM_OC1PreloadConfig(TIM14,TIM_OCPreload_Enable);
		//定时器使能
		TIM_Cmd(TIM14,ENABLE);

}


