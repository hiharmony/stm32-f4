#ifndef _LED_H
#define _LED_H
#include "stm32f4xx.h" 

#define LED0_ON GPIO_ResetBits(GPIOF,GPIO_Pin_9);
#define LED0_OFF GPIO_SetBits(GPIOF,GPIO_Pin_9);

#define LED1_ON GPIO_ResetBits(GPIOF,GPIO_Pin_10);
#define LED1_OFF GPIO_SetBits(GPIOF,GPIO_Pin_10);

void LED_Init(void);



#endif


