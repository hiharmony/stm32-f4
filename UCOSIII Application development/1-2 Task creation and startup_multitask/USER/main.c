#include "sys.h"
#include <bsp.h>
#include "includes.h"

//任务栈空间
#define LED0_TASK_STK_SIZE 128
#define LED1_TASK_STK_SIZE 128


static CPU_STK Led0TaskStk[LED0_TASK_STK_SIZE];
static CPU_STK Led1TaskStk[LED1_TASK_STK_SIZE];
//任务控制块
static OS_TCB Led0TaskTCB;
static OS_TCB Led1TaskTCB;
//任务函数声明
void LED0_Task (void* parameter);
void LED1_Task (void* parameter);
//任务优先级
#define LED0_TASK_PRIO 3
#define LED1_TASK_PRIO 3

int main(void)
{
      OS_ERR  err;
			BSP_Init(); //板级硬件初始化
      OSInit(&err); //初始化UCOS
      /* 创建任务*/
			OSTaskCreate( (OS_TCB     *)&Led0TaskTCB,      															//任务控制块                
                    (CPU_CHAR   *)"LED0",                													//任务名称
                    (OS_TASK_PTR ) LED0_Task,                  										//任务入口函数
                    (void       *) 0,                                      				//任务入口函数形参
                    (OS_PRIO     ) LED0_TASK_PRIO,           								//任务的优先级
                    (CPU_STK    *)&Led0TaskStk[0],                     						//指向栈基址的指针
                    (CPU_STK_SIZE) LED0_TASK_STK_SIZE / 10, 											//设置栈深度的限制位置   
                    (CPU_STK_SIZE) LED0_TASK_STK_SIZE,        										//任务栈大小
                    (OS_MSG_QTY  ) 5u,                            								//设置可以发送到任务的最大消息数
                    (OS_TICK     ) 0u,                          									//在任务之间循环时的时间片的时间量（以滴答为单位）
                    (void       *) 0,                                   					//指向用户提供的内存位置的指针
                    (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),    //用户可选的任务特定选项
                    (OS_ERR     *)&err);          																//用于保存返回的错误代码
			OSTaskCreate( (OS_TCB     *)&Led1TaskTCB,      															//任务控制块                
                    (CPU_CHAR   *)"LED1",                													//任务名称
                    (OS_TASK_PTR ) LED1_Task,                  										//任务入口函数
                    (void       *) 0,                                      				//任务入口函数形参
                    (OS_PRIO     ) LED1_TASK_PRIO,           											//任务的优先级
                    (CPU_STK    *)&Led1TaskStk[0],                     						//指向栈基址的指针
                    (CPU_STK_SIZE) LED1_TASK_STK_SIZE / 10, 											//设置栈深度的限制位置   
                    (CPU_STK_SIZE) LED1_TASK_STK_SIZE,        										//任务栈大小
                    (OS_MSG_QTY  ) 5u,                            								//设置可以发送到任务的最大消息数
                    (OS_TICK     ) 0u,                          									//在任务之间循环时的时间片的时间量（以滴答为单位）
                    (void       *) 0,                                   					//指向用户提供的内存位置的指针
                    (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),    //用户可选的任务特定选项
                    (OS_ERR     *)&err);          																//用于保存返回的错误代码
      /* 创建任务*/
      OSStart(&err); //启动UCOS
			while(1);
}


void LED0_Task(void* p_arg)
{
		OS_ERR err;
    while (1)                                      
    {
        LED0_ON;
        OSTimeDlyHMSM(0,0,0,500,OS_OPT_TIME_HMSM_STRICT,&err); //延时500ms
        LED0_OFF;
        OSTimeDlyHMSM(0,0,0,500,OS_OPT_TIME_HMSM_STRICT,&err); //延时500ms

    }
}


void LED1_Task(void* p_arg)
{
		OS_ERR err;
    while (1)                                      
    {
        LED1_ON;
        OSTimeDlyHMSM(0,0,0,500,OS_OPT_TIME_HMSM_STRICT,&err); //延时500ms
        LED1_OFF;
        OSTimeDlyHMSM(0,0,0,500,OS_OPT_TIME_HMSM_STRICT,&err); //延时500ms

    }
}

