#include <includes.h>
#include <string.h>
#include "led.h"

//OS_MEM  mem;                    //声明内存管理对象
//uint8_t ucArray [ 3 ] [ 20 ];   //声明内存分区大小

#define APP_TASK_START_STK_SIZE 128
#define APP_TASK_LED1_STK_SIZE 128
#define APP_TASK_LED2_STK_SIZE 128


#define APP_TASK_START_PRIO 1
#define APP_TASK_LED1_PRIO 2
#define APP_TASK_LED2_PRIO 2


static  OS_TCB   AppTaskStartTCB;    //任务控制块
static  OS_TCB   AppTaskLed1TCB;
static  OS_TCB   AppTaskLed2TCB;



static  CPU_STK  AppTaskStartStk[ APP_TASK_START_STK_SIZE];       //任务栈
static  CPU_STK  AppTaskLed1Stk [ APP_TASK_LED1_STK_SIZE ];
static  CPU_STK  AppTaskLed2Stk [ APP_TASK_LED2_STK_SIZE ];



static  void  AppTaskStart  (void *p_arg);               //任务函数声明
static  void  AppTaskLed1  ( void * p_arg );
static  void  AppTaskLed2  ( void * p_arg );



int  main (void)
{
    OS_ERR  err;
    OSInit(&err);                       //初始化 μC/OS-III

    /* 创建起始任务 */
    OSTaskCreate((OS_TCB     *)&AppTaskStartTCB,       //任务控制块地址
                (CPU_CHAR   *)"App Task Start",            //任务名称
                (OS_TASK_PTR ) AppTaskStart,              //任务函数
                (void       *) 0,
                //传递给任务函数（形参p_arg）的实参
                (OS_PRIO     ) APP_TASK_START_PRIO,  //任务的优先级
                (CPU_STK    *)&AppTaskStartStk[0],
                //任务栈的基地址
                (CPU_STK_SIZE) APP_TASK_START_STK_SIZE / 10,
                //任务栈空间剩下1/10时限制其增长
                (CPU_STK_SIZE) APP_TASK_START_STK_SIZE,
                //任务栈空间（单位：sizeof(CPU_STK)）
                (OS_MSG_QTY  ) 5u,
                //任务可接收的最大消息数
                (OS_TICK     ) 0u,
                //任务的时间片节拍数（0表默认值OSCfg_TickRate_Hz/10）
                (void       *) 0,
                //任务扩展（0表不扩展）
                (OS_OPT   )(OS_OPT_TASK_STK_CHK |OS_OPT_TASK_STK_CLR),//任务选项
                (OS_ERR     *)&err);                      //返回错误类型

    OSStart(&err);
    //启动多任务管理（交由μC/OS-III控制）
		while(1);
}

static  void  AppTaskStart (void *p_arg)
{
    CPU_INT32U  cpu_clk_freq;
    CPU_INT32U  cnts;
    OS_ERR      err;

    (void)p_arg;

    BSP_Init();               //板级初始化
    CPU_Init();
    //初始化 CPU组件（时间戳、关中断时间测量和主机名）

//    cpu_clk_freq = BSP_CPU_ClkFreq();
    //获取 CPU内核时钟频率（SysTick 工作时钟）
    cnts = cpu_clk_freq / (CPU_INT32U)OSCfg_TickRate_Hz;
    //根据用户设定的时钟节拍频率计算 SysTick 定时器的计数值
    OS_CPU_SysTickInit(cnts);
    //调用 SysTick初始化函数，设置定时器计数值和启动定时器

    Mem_Init();
    //初始化内存管理组件（堆内存池和内存池表）

#if OS_CFG_STAT_TASK_EN > 0u
//如果启用（默认启用）了统计任务
    OSStatTaskCPUUsageInit(&err);
    //计算没有应用任务（只有空闲任务）运行时 CPU的（最大）容量（决定OS_Stat_IdleCtrMax的值，为后面计算 CPU使用率使用）。
#endif
//    CPU_IntDisMeasMaxCurReset();
//    //复位（清零）当前最大关中断时间

//    /* 配置时间片轮转调度 */
//    OSSchedRoundRobinCfg((CPU_BOOLEAN   )DEF_ENABLED, //启用时间片轮转调度
//                        (OS_TICK       )0,  //把 OSCfg_TickRate_Hz / 10设为默认时间片值
//                        (OS_ERR       *)&err ); //返回错误类型


    /* 创建 LED1 任务 */
    OSTaskCreate((OS_TCB     *)&AppTaskLed1TCB,            //任务控制块地址
                (CPU_CHAR   *)"App Task Led1",
                (OS_TASK_PTR ) AppTaskLed1,                //任务函数
                (void       *) 0,
                //传递给任务函数（形参p_arg）的实参
                (OS_PRIO     ) APP_TASK_LED1_PRIO,//任务的优先级
                (CPU_STK    *)&AppTaskLed1Stk[0],
                //任务栈的基地址
                (CPU_STK_SIZE) APP_TASK_LED1_STK_SIZE / 10,
                //任务栈空间剩下1/10时限制其增长
                (CPU_STK_SIZE) APP_TASK_LED1_STK_SIZE,
                //任务栈空间（单位：sizeof(CPU_STK)）
                (OS_MSG_QTY  ) 5u,
                //任务可接收的最大消息数
                (OS_TICK     ) 0u,
                //任务的时间片节拍数（0表默认值）
                (void       *) 0,
                //任务扩展（0表不扩展）
                (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                (OS_ERR     *)&err);                    //返回错误类型

    /* 创建 LED2 任务 */
    OSTaskCreate((OS_TCB     *)&AppTaskLed2TCB, //任务控制块地址
                (CPU_CHAR   *)"App Task Led2",           //任务名称
                (OS_TASK_PTR ) AppTaskLed2,                //任务函数
                (void       *) 0,
                //传递给任务函数（形参p_arg）的实参
                (OS_PRIO     ) APP_TASK_LED2_PRIO,        //任务的优先级
                (CPU_STK    *)&AppTaskLed2Stk[0],
                //任务栈的基地址
                (CPU_STK_SIZE) APP_TASK_LED2_STK_SIZE / 10,
                //任务栈空间剩下1/10时限制其增长
                (CPU_STK_SIZE) APP_TASK_LED2_STK_SIZE,
                //任务栈空间（单位：sizeof(CPU_STK)）
                (OS_MSG_QTY  ) 5u,
                //任务可接收的最大消息数
                (OS_TICK     ) 0u,
                //任务的时间片节拍数（0表默认值）
                (void       *) 0,
                //任务扩展（0表不扩展）
                (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                (OS_ERR     *)&err);                //返回错误类型


    OSTaskDel ( 0, & err );        //删除起始任务本身，该任务不再运行
}


/*
************************************************************************
****
*                                          LED1 TASK
************************************************************************
****
*/

static  void  AppTaskLed1 ( void * p_arg )
{
    OS_ERR      err;
    OS_REG      value;

    (void)p_arg;

    while (DEF_TRUE)                  //任务体，通常写成一个死循环
    {
        LED0_Toggle;             //切换 LED1 的亮灭状态

        value = OSTaskRegGet ( 0, 0, & err ); //获取自身任务寄存器值

        if ( value < 10 )                  //如果任务寄存器值<10
        {
            OSTaskRegSet ( 0, 0, ++ value, & err );//继续累加任务寄存器值
        }
        else//如果累加到10
        {
            OSTaskRegSet ( 0, 0, 0, & err );        //将任务寄存器值归0

            OSTaskResume ( & AppTaskLed2TCB, & err );   //恢复 LED2 任务
            printf("恢复LED2任务！\n");


        }

        OSTimeDly ( 1000, OS_OPT_TIME_DLY, & err );
        //相对性延时1000个时钟节拍（1s）

    }

}


/*
***********************************************************************
****
*                                  LED2 TASK
***********************************************************************
****
*/

static  void  AppTaskLed2 ( void * p_arg )
{
    OS_ERR      err;
    OS_REG      value;


    (void)p_arg;


    while (DEF_TRUE)                   //任务体，通常写成一个死循环
    {
        LED1_Toggle;               //切换 LED2 的亮灭状态

        value = OSTaskRegGet ( 0, 0, & err ); //获取自身任务寄存器值

        if ( value < 5 )                     //如果任务寄存器值<5
        {
            OSTaskRegSet ( 0, 0, ++ value, & err );  //继续累加任务寄存器值
        }
        else//如果累加到5
        {
            OSTaskRegSet ( 0, 0, 0, & err );    //将任务寄存器值归0

            OSTaskSuspend ( 0, & err );                    //挂起自身
            printf("挂起LED2任务（自身）！\n");
        }

        OSTimeDly ( 1000, OS_OPT_TIME_DLY, & err );
        //相对性延时1000个时钟节拍（1s）

    }


}



