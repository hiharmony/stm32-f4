#include "key.h"
#include "stm32f4xx.h" 


void KEY_Init(void)
{
		GPIO_InitTypeDef GPIO_InitStructure;
		//时钟使能
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA,ENABLE);
		//按键初始化
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0; 
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF; //输入模式
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN; //按键接电源，所以这里接下拉
		GPIO_InitStructure.GPIO_Speed = GPIO_Fast_Speed;
		GPIO_Init(GPIOA,&GPIO_InitStructure);
		GPIO_PinAFConfig(GPIOA,GPIO_PinSource0,GPIO_AF_TIM5);
		
}








