#include "sys.h"
#include "delay.h"
#include "usart.h"
#include "led.h"
#include "lcd.h"
#include "snakeinfomation.h"
#include "snake.h"
#include "draw.h"
#include "rng.h"
#include "tim.h"
#include "key.h"
unsigned char menu=0; //界面 0:开始界面(难度选择) 1:游戏界面 2:结束分数界面
unsigned char mode=0; //难度模式：0normal正常 1medium中等 2hard难


int main(void)
{ 

		NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);//设置系统中断优先级分组2
		delay_init(168);      //初始化延时函数
		uart_init(115200);		//初始化串口波特率为115200
		TIM1_Init();
		keyInit();
		LED_Init();					  //初始化LED
		LCD_Init();           //初始化LCD FSMC接口
		while(RNG_Init())	 		//初始化随机数发生器
		{
			LCD_ShowString(30,130,200,16,16,"  RNG Error! ");	 
			delay_ms(200);
			LCD_ShowString(30,130,200,16,16,"RNG Trying...");	 
		}     
		LCD_Clear(BLACK);
		snakeInit();
		//drawGameInit();
    while(1)
    {
				
			if(game_Scan) //游戏运行
			{
					game_Scan=0;
					switch(menu) {
							case 0: //开始界面(难度选择)
									drawGameInit();
							break;
							case 1: //游戏界面
									LED0=!LED0;	
									judgeDir(); //方向
									snakeForward();
									refreshMap();
							break;
							case 2: //结束界面
									drawGameEnd();
							break;
					}
			}
			if(key_Scan) //按键扫描
			{
					key_Scan=0;
					keyScan();
			}
    }
}
