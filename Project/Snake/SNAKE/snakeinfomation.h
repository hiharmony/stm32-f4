#ifndef SNAKEINFOMATION_H
#define SNAKEINFOMATION_H

/*---游戏地图窗口---*/
#define MAPWIDTH 240 //游戏地图宽度 20 横坐标1=2格 纵坐标1=1格
#define MAPHEIGHT 240 //高度地图高度 20


/*---结构体---*/
typedef struct{
        unsigned char x;
        unsigned char y;
}SnakeBodyCoord;//蛇身坐标coordinate结构体


typedef struct{
        SnakeBodyCoord snakeBody[(MAPWIDTH/10-2)*(MAPHEIGHT/10-2)]; //0位是蛇头，高位是蛇身
        unsigned int length;         //蛇身长度
}SnakeBody; //蛇身信息结构体 顺序线性表

typedef struct
{
        unsigned char x;
        unsigned char y;        //食物坐标
        unsigned char isFood;   //是否绘制食物 0否，1是
}Food; //食物信息结构体

typedef struct
{
    unsigned char modeDegree;
    unsigned char modeNextDegree;
    unsigned char modeBackDegree;
    unsigned char *modeString;
    unsigned int sleeptime;
		unsigned int color;
}Mode; //游戏难度信息结构体

extern SnakeBody mySnake; //全局变量，蛇
extern Food food;         //全局变量，食物

extern Mode modeEnum[3];               //游戏难度枚举
extern unsigned char menu;             //界面 0:开始界面(难度选择) 1:游戏界面 2:结束分数界面
extern unsigned char mode;             //难度模式：0normal正常 1medium中等 2hard难

//扫描频率
extern unsigned char key_Scan;
extern unsigned char game_Scan;







#endif // SNAKEINFOMATION_H

