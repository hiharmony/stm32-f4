#include "snakeinfomation.h"
#include "draw.h"
#include "lcd.h"
#include "snake.h"
#include "key.h"


void drawGameInit(void)
{
		u8 x,y;

		x=(240-12*10)/2;
		POINT_COLOR=WHITE;	
		y=mySnake.snakeBody[0].y*Y_SQUARE+Y_ERROR;
		LCD_ShowString(x,y,200,24,24,"Game:Snake");
		LCD_ShowString(x,y+25,100,16,16,"Mode:");
		LCD_ShowString(x,y+25+20,200,12,12,"Please press key \"0\"...");
		POINT_COLOR=modeEnum[mode].color;	  
		LCD_ShowString(x+16*3,y+25,100,16,16,modeEnum[mode].modeString);
		while(1)
		{
					if(key_Scan)
					{
							key_Scan=0;
							keyScan();
					}
					if(keySta[0]) //上
					{
							keySta[0]=0;
							LCD_Fill(x+16*3,y+25,x+16*3+50,y+25+16,BLACK);
							mode = modeEnum[mode].modeBackDegree;
							POINT_COLOR=modeEnum[mode].color;	  
							LCD_ShowString(x+16*3,y+25,100,16,16,modeEnum[mode].modeString);
					}
					if(keySta[1]) //下
					{
							keySta[1]=0;
							LCD_Fill(x+16*3,y+25,x+16*3+50,y+25+16,BLACK);

							mode = modeEnum[mode].modeNextDegree;
							POINT_COLOR=modeEnum[mode].color;	  
							LCD_ShowString(x+16*3,y+25,100,16,16,modeEnum[mode].modeString);
					}
					if(keySta[3])
					{
							LCD_Clear(BLACK);
							drawMapInit();
							refreshMap();
							menu=1;
							break;
					}
						
		}
		//while(1);
}


// 画静态地图 边框
void drawMapInit(void)
{
		u8 i;
		//画墙
		POINT_COLOR=WHITE;	  
		for(i=0;i<24;i++) //画墙壁
		{
				LCD_DrawRectangle(0+i*10,0,10+i*10,10);
				LCD_DrawRectangle(0+i*10,230,10+i*10,240);//画横
				LCD_DrawRectangle(0,0+i*10,10,10+i*10);
				LCD_DrawRectangle(230,0+i*10,240,10+i*10);//画竖的
		}
		POINT_COLOR=BLUE;	  
		LCD_ShowString(0,260,210,24,24,"Score:");
}

//画动态图
void refreshMap(void)
{
		unsigned int i;
	  //画蛇身
    //POINT_COLOR=GREEN;	  
    for(i=0;i<mySnake.length;i++)
    {
        LCD_Fill_Circle(mySnake.snakeBody[i].x*X_SQUARE+X_ERROR,mySnake.snakeBody[i].y*Y_SQUARE+Y_ERROR,SANKE_R,GREEN); //相对坐标转化为绝对坐标
    }
    if(0==food.isFood)
    {
        //画食物
        LCD_Fill_Circle(food.x*X_SQUARE+X_ERROR,food.y*Y_SQUARE+Y_ERROR,FOOD_R,RED); //相对坐标转化为绝对坐标
        food.isFood=1; //食物已绘制
    }
    //画分数
		POINT_COLOR=YELLOW;	
		LCD_Fill(30+8*11,260,30+8*11+50,260+24,BLACK);
		LCD_ShowNum(30+8*11,260,mySnake.length-3,4,24); 

		
}


void drawGameEnd(void)
{
		u8 x,y;
		POINT_COLOR=RED;	
		x=(240-12*10)/2;
		y=mySnake.snakeBody[0].y*Y_SQUARE+Y_ERROR;
		LCD_ShowString(x,y,200,24,24,"Game Over");
		while(1)
		{
					if(key_Scan)
					{
							key_Scan=0;
							keyScan();
					}
					if(keySta[3])
					{
							LCD_Clear(BLACK);
							snakeInit();
							drawMapInit();
							refreshMap();
							menu=1;
							break;
					}
						
		}
}


