#include <stdlib.h>
#include "snakeinfomation.h"
#include "snake.h"
#include "draw.h"
#include "rng.h"
#include "lcd.h"
#include "key.h"

int Dir[4][2]={{0,-1},{0,+1},{-1,0},{1,0}}; //上下左右
Mode modeEnum[3]={{0,1,2,"normal",500,GREEN},{1,2,0,"medium",300,YELLOW},{2,0,1,"hard",150,RED}};
SnakeBody mySnake;
Food food;
unsigned char foodEnable; //食物不能刷在蛇的身体上
unsigned int snakeStart=0;
unsigned char keyDir=3; //上0下1左2右3，初始方向向右

void snakeInit(void)
{
		//蛇身参数初始化 使用相对坐标横坐标1=2格 纵坐标1=1格
    mySnake.snakeBody[0].x=(unsigned char)((MAPWIDTH/10-2)/2);   //蛇头，正中央
    mySnake.snakeBody[0].y=(unsigned char)((MAPHEIGHT/10-2)/2);
    mySnake.snakeBody[1].x=(unsigned char)((MAPWIDTH/10-2)/2)-1; //往左一位
    mySnake.snakeBody[1].y=(unsigned char)((MAPHEIGHT/10-2)/2);
    mySnake.snakeBody[2].x=(unsigned char)((MAPWIDTH/10-2)/2)-2; //往左两位
    mySnake.snakeBody[2].y=(unsigned char)((MAPHEIGHT/10-2)/2);
    mySnake.length = 3; //长度为3
		getFoodCoord();
		keyDir=3;
}


//随机生成食物
void getFoodCoord(void)
{
		unsigned int i;
		u32 random;
		random=RNG_Get_RandomNum(); //获得随机数
    srand(random);
    while (1)
    {
        foodEnable=1;

        food.x = rand() % (22);   //20
        food.y = rand() % (22);  //20
        //生成的食物横坐标的奇偶必须和初试时蛇头所在坐标的奇偶一致，因为一个字符占两个字节位置，若不一致
        //会导致吃食物的时候只吃到一半
        for(i=0;i<mySnake.length;i++) //遍历蛇身判断是否在蛇身上
        {
            if((mySnake.snakeBody[i].x == food.x)&&(mySnake.snakeBody[i].y == food.y)) //两坐标重合
                foodEnable=0; //有重合则不通过
        }
        if((food.x!=0)&&(food.y!=0)&&(foodEnable==1))
            break;
    }
    food.isFood=0; //食物被吃掉，新的坐标数据已生成，等待被画
}


//判断是否撞墙和撞身体 0没有1撞墙2撞身体
unsigned char judgeWallAndSnakeBody(unsigned char dir)
{
		unsigned int i;
    switch (dir) { //判断撞墙
        //一开始我想是不是只要判断在不在边缘不用判断方向，后来想到到边缘也是可以换方向的，所以必须判断方向
        case 0: //方向向上，判断y方向是不是1
            if(mySnake.snakeBody[0].y==1)
            {
                return 1;
            }
        break;
        case 1: //方向向下，判断y方向是不是20
            if(mySnake.snakeBody[0].y==22)
            {
                return 1;
            }
        break;
        case 2: //方向向左，判断y方向是不是1
            if(mySnake.snakeBody[0].x==1)
            {
                return 1;
            }
        break;
        case 3: //方向向右，判断x方向是不是20
            if(mySnake.snakeBody[0].x==22)
            {
                return 1;
            }
        break;
    }

    for(i=0;i<mySnake.length;i++) //遍历蛇身判断是否撞蛇身
    {
        if( ((mySnake.snakeBody[0].x+Dir[dir][0]) == mySnake.snakeBody[i].x)&&((mySnake.snakeBody[0].y+Dir[dir][1]) == mySnake.snakeBody[i].y) ) //两坐标重合
            return 2;
    }
    return 0;
}

//判断是否吃到食物 0没有1有
unsigned char judgeFood(unsigned char dir)
{
    if( ((mySnake.snakeBody[0].x+Dir[dir][0]) == food.x)&&((mySnake.snakeBody[0].y+Dir[dir][1]) == food.y) ) //两坐标重合
        return 1;
    return 0;
}



void snakeForward(void)
{
		unsigned int i;
		if(judgeWallAndSnakeBody(keyDir))
		{
				menu=2;//死掉，游戏结束
		}
		if(2!=menu)
		{
				if(judgeFood(keyDir))
				{
						mySnake.length+=1; //体长+1
						snakeStart=mySnake.length;
						LCD_Fill_Circle(food.x*X_SQUARE+X_ERROR,food.y*Y_SQUARE+Y_ERROR,FOOD_R,BLACK); //食物刷掉
						getFoodCoord();//随机生成食物
				}
				else
				{
						snakeStart=mySnake.length-1;
						LCD_Fill_Circle(mySnake.snakeBody[snakeStart].x*X_SQUARE+X_ERROR,mySnake.snakeBody[snakeStart].y*Y_SQUARE+Y_ERROR,SANKE_R,BLACK);
				}
				//正常前进 进头去尾 吃掉食物 保留尾部
        for(i=snakeStart;i>0;i--)
        {
            mySnake.snakeBody[i]=mySnake.snakeBody[i-1];
        }
        mySnake.snakeBody[0].x+=Dir[keyDir][0];
        mySnake.snakeBody[0].y+=Dir[keyDir][1]; //头向前进一步
		}
}



void judgeDir(void)
{

		if((1==keySta[0])&&keyDir!=1)        //按键为向上且当前状态不为向下
    {
				keySta[0]=0;
        keyDir=0;
    }
    else if((1==keySta[1])&&keyDir!=0)   //按键为向下且当前状态不为向上
    {
				keySta[1]=0;
        keyDir=1;
    }
    else if((1==keySta[2])&&keyDir!=3)   //按键为向左且当前状态不为向右
    {
				keySta[2]=0;
        keyDir=2;
    }
    else if((1==keySta[3])&&keyDir!=2)   //按键为向右且当前状态不为向左
    {
				keySta[3]=0;
        keyDir=3;
    }
		
}


