#ifndef DRAW_H
#define DRAW_H

#define X_SQUARE 10
#define Y_SQUARE 10

#define X_ERROR 5
#define Y_ERROR 5

#define SANKE_R 4
#define FOOD_R 2

void drawGameInit(void);
void drawMapInit(void);
void refreshMap(void);
void drawGameEnd(void);


#endif // DRAW_H


