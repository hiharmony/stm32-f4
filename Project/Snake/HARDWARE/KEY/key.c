#include "key.h"
#include "stm32f4xx.h" 
#include "sys.h"

u8 keySta[4]={0,0,0,0};


void keyInit(void)
{
		GPIO_InitTypeDef GPIO_InitStructure;
		//时钟使能
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOE,ENABLE);
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA,ENABLE);
		//按键初始化
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2|GPIO_Pin_3|GPIO_Pin_4; 
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN; //输入模式
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP; //按键接地，所以这里接上拉
		GPIO_InitStructure.GPIO_Speed = GPIO_Fast_Speed;
		GPIO_Init(GPIOE,&GPIO_InitStructure);
		//按键初始化
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0; 
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN; //输入模式
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN; //按键接高，所以这里接下拉
		GPIO_InitStructure.GPIO_Speed = GPIO_Fast_Speed;
		GPIO_Init(GPIOA,&GPIO_InitStructure);
}

void keyScan(void)
{
		u8 i;
		static u8 keyBuff[4]={0,0,0,0}; //上下左右
		static u8 keyStaBuff[4]={0,0,0,0}; //弹起检测
			
		
		for(i=0;i<4;i++)
		{
				if(1==keyBuff[i])
				{
						keyBuff[i]=0;
						keyStaBuff[i]=1;
				}
		}		
		

		keyBuff[0]=PAin(0);
		keyBuff[1]=!PEin(3);
		keyBuff[2]=!PEin(2);
		keyBuff[3]=!PEin(4);
		
		for(i=0;i<4;i++)
		{
				if((1==keyStaBuff[i])&&(0==keyBuff[i]))
				{
						keyStaBuff[i]=0;
						keySta[i]=1;
				}
		}	
	
}

