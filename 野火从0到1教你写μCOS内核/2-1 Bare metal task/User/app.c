#include "stm32f10x.h"
/* flag 必须定义成全局变量才能添加到逻辑分析仪里面观察波形
*在逻辑分析仪中要设置以 bit 的模式才能看到波形，不能用默认的模拟量
*/
uint32_t flag1;
uint32_t flag2;


/* 软件延时，不必纠结具体的时间 */
void delay( uint32_t count )
{
    for (; count!=0; count--);
}

int main(void)
{
    /* 无限循环，顺序执行 */
    for (;;)
    {
        flag1 = 1;
        delay( 100 );
        flag1 = 0;
        delay( 100 );

        flag2 = 1;
        delay( 100 );
        flag2 = 0;
        delay( 100 );
    }
}




